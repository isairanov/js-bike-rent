'use strict';
function init() {
  // вызови функцию loadCatalog для загрузки первой страницы каталога
  let currentPage = 1;
  loadCatalog(currentPage);
  // Реализуй и установи обработчик нажатия на кнопку "Загрузить еще"
  document.getElementById('loadMore')
    .addEventListener('click', function () {
      loadCatalog(++currentPage);
    });
}

async function loadCatalog(page) {
  // Здесь необходимо сделать загрузку каталога (api.getBikes)
  // и передать полученные данные в функции appendCatalog и showButtonLoadMore
  let catalog = await api.getBikes(getPointId(), page);
  console.log(catalog);
  appendCatalog(catalog);
  showButtonLoadMore(catalog.hasMore);
}

function appendCatalog(items) {
  // отрисуй велосипеды из items в блоке <div id="bikeList">
  const bikes = items.bikesList;
  const bikeList = document.getElementById('bikeList');
  bikes.forEach(item => {
    let bikeContent = document.createElement('div');
    bikeContent.classList.add('bike-content');
    bikeList.appendChild(bikeContent);

    let imgBlock = document.createElement('div');
    imgBlock.classList.add('image-block');
    bikeContent.appendChild(imgBlock);

    let img = document.createElement('img');
    img.src = `/images/${item.img}`;
    img.classList.add('bike-img');
    imgBlock.appendChild(img);

    let title = document.createElement('span');
    title.classList.add('bike-title');
    title.textContent = item.name;

    let text = document.createElement('span');
    text.classList.add('bike-text');
    text.textContent = `Стоимость за час - ${item.cost * 60} ₽`;

    let button = document.createElement('button');
    button.classList.add('button', 'rent');

    let link = document.createElement('a');
    button.appendChild(link);
    link.classList.add('rent-link');
    link.textContent = `Арендовать`;

    button.addEventListener('click', event => {
      event.preventDefault();
      window.location.href = `/order/${item._id}?prevurl=${location.href}`;
    });
    bikeContent.append(title, text, button);
  })
}

function showButtonLoadMore(hasMore) {
  // если hasMore == true, то показывай кнопку #loadMore
  // иначе скрывай
  hasMore ? enableButtonLoadMore() : disableButtonLoadMore();
}

function disableButtonLoadMore() {
  // заблокируй кнопку "загрузить еще"
  document.getElementById('loadMore').classList.add('hidden');
}

function enableButtonLoadMore() {
  // разблокируй кнопку "загрузить еще"
  document.getElementById('loadMore').classList.remove('hidden');
}

function getPointId() {
  // сделай определение id выбранного пункта проката
  let url = document.URL.split('/');
  return url[url.length - 1] === 'catalog' ? '' : url[url.length - 1];
}


document.addEventListener('DOMContentLoaded', init)
