const requisites = document.getElementsByClassName('requisites-input');
let isBlurError1 = false;
let isBlurError2 = false;
let isBlurError3 = false;


function cardNumberValidate(cardNumberValue) {
  if (cardNumberValue.value.replace(/\s+/g, '').length !== 16 && cardNumberValue !== '') {
    renderError(1, 'номер должен содержать 16 цифр', requisites[0]);
    isBlurError1 = true;
  } else {
    hideError(1);
    cardNumberValue.value = cardNumberValue.value.replace(/\s+/g, '').replace(/(\d)(?=(\d{4})+(\D|$))/g, '$1 ');
    isBlurError1 = false;
  }
}

function cardDateValidation(cardDateValue) {
  cardDateValue = cardDateValue.replace(/\s+/g, '');
  if (cardDateValue.length !== 4 && cardDateValue !== '') {
    renderError(2, 'дата должна содержать 4 цифры', requisites[1]);
    isBlurError2 = true;
    return;
  }

  let year = cardDateValue.split(/(?=(?:\d{2})+(?!\d))/)[1];

  let currentDate = new Date();
  if ((year < (currentDate.getFullYear() % 100)) || (year > (currentDate.getFullYear() % 100) + 5)) {
    renderError(2, 'год не может быть меньше текущего или больше текущего на 5 лет', requisites[1]);
    isBlurError2 = true;
    return;
  } else {
    isBlurError2 = false;
  }

  let month = cardDateValue.split(/(?=(?:\d{2})+(?!\d))/)[0];

  if (month > 12 || month < 1) {
    renderError(2, 'месяц не существует', requisites[1]);
    isBlurError2 = true;
    return;
  } else {
    hideError(2);
    isBlurError2 = false;
  }
  if (cardDateValue !== '') requisites[1].value = month + '/' + year;
}

function cvvValidate(cardCvvValue) {
  if (cardCvvValue.replace(/\s+/g, '').length !== 3 && cardCvvValue !== '') {
    renderError(3, 'номер должен содержать 3 цифры', requisites[2]);
    isBlurError3 = true;
  } else {
    hideError(3);
    isBlurError3 = false;
  }
}

function renderError(toolTipDataId, error) {
  requisites[toolTipDataId - 1].style.borderColor = 'red';
  switch (toolTipDataId) {
    case 1: {
      let tip = document.getElementById('number-hexagon');
      tip.style.display = 'flex';
      let tipText = document.getElementById('number-text');
      tipText.innerHTML = error;
      break;
    }
    case 2: {
      let tip = document.getElementById('date-hexagon');
      tip.style.display = 'flex';
      let tipText = document.getElementById('date-text');
      tipText.innerHTML = error;
      break;
    }
    case 3: {
      let tip = document.getElementById('cvv-hexagon');
      tip.style.display = 'flex';
      let tipText = document.getElementById('cvv-text');
      tipText.innerHTML = error;
      break;
    }
  }
}

function hideError(toolTipDataId, elem) {
  switch (toolTipDataId) {
    case 1: {
      if (isBlurError1) return;
      requisites[toolTipDataId - 1].style.borderColor = '#ccc';
      let tip = document.getElementById('number-hexagon');
      tip.style.display = 'none';
      break;
    }
    case 2: {
      if (isBlurError2) return;
      requisites[toolTipDataId - 1].style.borderColor = '#ccc';
      let tip = document.getElementById('date-hexagon');
      tip.style.display = 'none';
      break;
    }
    case 3: {
      if (isBlurError3) return;
      requisites[toolTipDataId - 1].style.borderColor = '#ccc';
      let tip = document.getElementById('cvv-hexagon');
      tip.style.display = 'none';
      break;
    }
  }
}

function init() {
  let i;
  for (i = 0; i < requisites.length; i++) {
    let formNumber = i;
    requisites[i].addEventListener("blur", function (event) {
      event.preventDefault();
      switch (formNumber) {
        case 0: {
          cardNumberValidate(requisites[formNumber]);
          break;
        }
        case 1: {
          requisites[formNumber].value = requisites[formNumber].value.replace(/[^0-9]/gim, '');
          cardDateValidation(requisites[formNumber].value);
          break;
        }
        case 2: {
          cvvValidate(requisites[formNumber].value);
          break;
        }
      }
    }, true);
  }
  document.getElementById('save-button').addEventListener('click', () => {
    for (let i = 0; i < requisites.length; i++) {
      if ((isNaN(Number(requisites[i].value.replace(/\s+/g, ''))))) {
        if (i === 1) continue;
        renderError(i + 1, 'поле должно содержать только цифры');
      } else if (requisites[i].value === '') {
        renderError(i + 1, 'поле не может быть пустым');
      } else hideError(i + 1);
    }
    let errorsNotFound = true;
    for (let i = 0; i < requisites.length; i++) {
      if (requisites[i].style.borderColor === 'red') {
        errorsNotFound = false;
      }
    }
    if (errorsNotFound) {
      const data = {
        number: requisites[0].value,
        date: requisites[1].value,
        cvv: requisites[2].value
      };
      const options = {
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data),
        credentials: 'include'
      };
      fetch('/api/card-requisites', options).then(response => {
        if (response.ok) {
          console.log('Данные отправлены');
          document.location.href = '/lk';
        } else {
          throw new Error('Ошибка при отправлении реквизитов карты');
        }
      })
    }
  });
}


document.addEventListener('DOMContentLoaded', init);
