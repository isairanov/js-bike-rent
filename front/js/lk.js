function init() {

  const changeButton = document.getElementById('card-change');
  changeButton.addEventListener('click', () => {
    window.location.href = "/card-requisites?prevurl=http://localhost:3000/lk";
  });

  const returnButtons = document.getElementsByClassName('return');

  for (let i = 0; i < returnButtons.length; i++) {
    returnButtons[i].addEventListener("click", function (event) {

      const bikeId = event.target.dataset.orderId;
      const options = {
        method: "DELETE",
        body: JSON.stringify(bikeId)
      };

      fetch(`/api/order/${bikeId}`, options).then(response => {
        if (response.ok) {
          const buttons = document.getElementsByClassName('return');
          for (let i = 0; i < buttons.length; i++) {
            if (buttons[i].getAttribute('data-order-id') === bikeId) {
              document.getElementById('lk-container__bikes').removeChild(buttons[i].parentNode.parentNode);
              window.location = location.href;
            }
          }
        } else {
          throw new Error('Ошибка запроса на удаление');
        }
      })
    })
  }
}

document.addEventListener('DOMContentLoaded', init);
